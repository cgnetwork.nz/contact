@if (session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
@endif

<form id="contact" method="post" action="{{ url('contact') }}">
    @csrf
    <div class="form-group">
        <input class="form-control" name="name" type="text" placeholder="Your Name" value="{{ old('name') }}">
        @if ($errors->has('name'))
            <p class="text-danger">{{ $errors->first('name') }}</p>
        @endif
    </div>

    <div class="form-group">
        <input class="form-control" name="email" type="text" placeholder="Your E-mail" value="{{ old('email') }}">
        @if ($errors->has('email'))
            <p class="text-danger">{{ $errors->first('email') }}</p>
        @endif
    </div>

    <div class="form-group">
        <textarea class="form-control" name="message" placeholder="Message" value="{{ old('message') }}"></textarea>
        @if ($errors->has('message'))
            <p class="text-danger">{{ $errors->first('message') }}</p>
        @endif
    </div>

    <div class="form-group">
        <div class="g-recaptcha" data-sitekey="{{ config('recaptcha') }}"></div>
        @if ($errors->has('g-recaptcha-response'))
            <p class="text-danger">The reCAPTCHA field is required.</p>
        @endif
    </div>

    <div class="form-group">
        <input type="submit" value="Send Message" class="btn btn-primary py-3 px-5">
    </div>
</form>