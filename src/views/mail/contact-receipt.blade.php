# Hi {{ $request->body()['name'] }}

<p>Thank you for contacting us. We have received your enquiry, and will be in touch as soon as possible.</p>

<p>In the meantime, please feel free to visit us again:</p>

@component('mail::button', ['url' => config('app.url')])
    Visit Us
@endcomponent