@component('mail::message')
@include($request->view())

Thank you,<br>
{{ config('app.name') }}
@endcomponent