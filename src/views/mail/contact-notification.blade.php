# Contact Details

@foreach($request->body() as $key => $value)
{{ ucfirst($key) }}
@component('mail::panel')
    {{ $value }}
@endcomponent
@endforeach