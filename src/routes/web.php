<?php
Route::group(['middleware' => 'web'], function () {
    Route::post('contact', 'Cgnetwork\Contact\App\Http\Controllers\ContactController@store')->name('contact.store');
});