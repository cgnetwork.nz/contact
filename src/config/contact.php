<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Mail From Address
    |--------------------------------------------------------------------------
    |
    | Here you may provide the default address for sending emails from.
    |
    */

    'address' => env('MAIL_FROM_ADDRESS', 'hello@example.com'),

    /*
    |--------------------------------------------------------------------------
    | Google reCAPTCHA Site Key - v2 Checkbox
    |--------------------------------------------------------------------------
    |
    | Here you may provide your client side integration site key for Google reCAPTCHA.
    |
    */

    'recaptcha' => env('RECAPTCHA_SITE_KEY', 'recaptchasitekey'),
    ];