<?php

namespace Cgnetwork\Contact\App\Repositories;

use Cgnetwork\Contact\App\Contracts\CanContact;
use Cgnetwork\Contact\App\Events\ContactReceived;
use Illuminate\Http\Request;

class ContactRepository implements CanContact
{
    public function validate(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email',
            'message' => 'required|max:255',
            'g-recaptcha-response' => 'required'
        ]);
    }

    public function dispatch(Request $request)
    {
        event(new ContactReceived($request));
    }
}