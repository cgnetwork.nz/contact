<?php

namespace Cgnetwork\Contact\App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Cgnetwork\Contact\App\Contracts\CanEmail;

class Mailer extends Mailable
{
    use Queueable, SerializesModels;

    public $request;

    public function __construct(CanEmail $request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->request->subject() . ' - ' . config('app.name'))->markdown('cgnetwork::mail.container');
    }
}
