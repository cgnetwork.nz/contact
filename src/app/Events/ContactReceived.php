<?php

namespace Cgnetwork\Contact\App\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class ContactReceived
{
    use Dispatchable, SerializesModels;

    public $request;

    public function __construct($request)
    {
        $this->request = $request->except(['_token', 'g-recaptcha-response']);
    }
}