<?php

namespace Cgnetwork\Contact\App\Contracts;

use Illuminate\Http\Request;

interface CanContact
{
    public function validate(Request $request);

    public function dispatch(Request $request);
}