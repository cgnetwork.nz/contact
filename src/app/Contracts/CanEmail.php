<?php

namespace Cgnetwork\Contact\App\Contracts;

interface CanEmail
{
    public function view(): string;

    public function recipients();

    public function subject(): string;

    public function body();
}