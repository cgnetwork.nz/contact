<?php

namespace Cgnetwork\Contact\App\Listeners;

use Cgnetwork\Contact\App\Contracts\CanEmail;
use Cgnetwork\Contact\App\Jobs\ProcessEmail;

class ContactReceipt implements CanEmail
{
    private $event;

    public function view(): string
    {
        return 'cgnetwork::mail.contact-receipt';
    }

    public function recipients()
    {
        return $this->event['email'];
    }

    public function subject(): string
    {
        return 'Thank You - Contact Receipt';
    }

    public function body()
    {
        return $this->event;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $this->event = $event->request;

        ProcessEmail::dispatch($this);
    }
}
