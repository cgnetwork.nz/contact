<?php

namespace Cgnetwork\Contact\App\Listeners;

use Cgnetwork\Contact\App\Contracts\CanEmail;
use Cgnetwork\Contact\App\Jobs\ProcessEmail;

class ContactNotification implements CanEmail
{
    private $event;

    public function view(): string
    {
        return 'cgnetwork::mail.contact-notification';
    }

    public function recipients()
    {
        return config('contact.address');
    }

    public function subject(): string
    {
        return $this->event['name'] . ' - Contact Received';
    }

    public function body()
    {
        return $this->event;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $this->event = $event->request;

        ProcessEmail::dispatch($this);
    }
}
