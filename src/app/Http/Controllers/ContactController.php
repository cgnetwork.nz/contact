<?php

namespace Cgnetwork\Contact\App\Http\Controllers;

use Cgnetwork\Contact\App\Repositories\ContactRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    private $contact;

    /**
     * ContactController constructor.
     * @param $contact
     */
    public function __construct(ContactRepository $contact)
    {
        $this->contact = $contact;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->contact->validate($request);

        $this->contact->dispatch($request);

        return redirect(url()->previous())->with('success', 'Success');
    }
}